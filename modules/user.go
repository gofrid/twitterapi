package modules

type User_details struct {
	Handle          string
	Url             string
	Timezone        string
	StatusesCount   int64
	Location        string
	CreatedAt       string
	AccountAge      float64
	FriendsCount    int
	FollowersCount  int
	FavouritesCount int
}
