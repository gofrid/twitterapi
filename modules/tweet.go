package modules

type Tweet_details struct {
	CreatedAt     string
	Age           float64
	FavoriteCount int
	RetweetCount  int
}
