package main

import (
	"api/auth"
	"api/modules"
	"api/tools"
	"fmt"
	"math"
	"net/url"
)

func main() {
	//https://godoc.org/github.com/amit-lulla/twitterapi
	//query API for specific handle
	user_output := searchProfile("Twitter")
	getPosts("Twitter")
	fmt.Println(user_output)
}

func searchProfile(profileName string) modules.User_details {
	api := auth.Connect()

	searchResultt, _ := api.GetSearch(profileName, nil)
	for _, tweet := range searchResultt.Statuses {
		fmt.Println(tweet.Text)
	}
	searchResult, _ := api.GetUsersShow(profileName, nil)
	fmt.Println(searchResult)

	FavouritesCount := searchResult.FavouritesCount
	fmt.Println("Favourites Count : ", FavouritesCount)

	FollowersCount := searchResult.FollowersCount
	fmt.Println("Followers Count : ", FollowersCount)

	FriendsCount := searchResult.FriendsCount
	fmt.Println("Friends Count : ", FriendsCount)

	CreatedAt := searchResult.CreatedAt
	fmt.Println("Created At : ", CreatedAt)

	Location := searchResult.Location
	fmt.Println("Location : ", Location)

	StatusesCount := searchResult.StatusesCount
	fmt.Println("Statuses Count : ", StatusesCount)

	TimeZone := searchResult.TimeZone
	fmt.Println("TimeZone : ", TimeZone)

	URL := searchResult.URL
	fmt.Println("URL : ", URL)

	handle := searchResult.Name
	fmt.Println("Handle : ", handle)

	acctAge := tools.CalcAge(CreatedAt)
	output := modules.User_details{
		Handle:          handle,
		Url:             URL,
		Timezone:        TimeZone,
		StatusesCount:   StatusesCount,
		Location:        Location,
		CreatedAt:       CreatedAt,
		AccountAge:      acctAge,
		FriendsCount:    FriendsCount,
		FollowersCount:  FollowersCount,
		FavouritesCount: FavouritesCount}

	return output
}

func getPosts(profileName string) {
	api := auth.Connect()
	v, _ := url.ParseQuery("screen_name=" + profileName + "&count=100&include_rts=False&tweet_mode=extended")
	searchResult, _ := api.GetUserTimeline(v)

	for _, value := range searchResult {
		CreatedAt := value.CreatedAt
		FavoriteCount := value.FavoriteCount
		RetweetCount := value.RetweetCount
		Posted := tools.CalcAge(CreatedAt)
		CreatedDate := tools.CleanDate(CreatedAt)

		rounded := math.Floor(Posted*100) / 100 //rounds number

		output := modules.Tweet_details{
			CreatedAt:     CreatedDate,
			Age:           rounded,
			FavoriteCount: FavoriteCount,
			RetweetCount:  RetweetCount,
		}
		print(output)
	}
}

// httpClient := auth.Auth()

// // Twitter client
// client := twitter.NewClient(httpClient)

// // Home Timeline
// tweets, resp, err := client.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
// 	Count: 20,
// })
// fmt.Println(tweets)

// // Send a Tweet
// // tweet, resp, err := client.Statuses.Update("just setting up my twttr", nil)
// // fmt.Println(tweet)
// // Status Show
// //tweet, resp, err := client.Statuses.Show(585613041028431872, nil)

// // Search Tweets
// search, resp, err := client.Search.Tweets(&twitter.SearchTweetParams{
// 	Query: "gopher",
// })
// fmt.Println(search)

// // User Show
// user, resp, err := client.Users.Show(&twitter.UserShowParams{
// 	ScreenName: "dghubble",
// })
// fmt.Println(user)

// // Followers
// followers, resp, err := client.Followers.List(&twitter.FollowerListParams{})
// fmt.Println(followers)
