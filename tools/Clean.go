package tools

import "strings"

func CleanDate(CreatedAt string) string {
	month := strings.Split(CreatedAt, " ")[1]
	day := strings.Split(CreatedAt, " ")[2]
	year := strings.Split(CreatedAt, " ")[5]

	if month == "Jan" {
		month = "01"
	} else if month == "Feb" {
		month = "02"
	} else if month == "Mar" {
		month = "03"
	} else if month == "Apr" {
		month = "04"
	} else if month == "May" {
		month = "05"
	} else if month == "Jun" {
		month = "06"
	} else if month == "Jul" {
		month = "07"
	} else if month == "Aug" {
		month = "08"
	} else if month == "Sep" {
		month = "09"
	} else if month == "Oct" {
		month = "10"
	} else if month == "Nov" {
		month = "11"
	} else if month == "Dec" {
		month = "12"
	}

	full_date := year + "-" + month + "-" + day

	return full_date
}
